#ifndef BURLINGTON_H
#define BURLINGTON_H

#include "Context.h"
#include "Distance.h"

using namespace std;

//const char L1 = 1;
//const char L2 = 2;
//const char Lsup = 3;


#define L1 1
#define L2 2
#define Lsup 3



template <class T>
class Burlington
{
	private:
		
	public:
		std::vector<Context<T> > BurConList;

		Context<T> & operator[](const int contextNumber);

		void newContext(char norm, long dimension);
		//accessors
		//interface
		/*void train(std::vector<T> tensor, long contextIdx);
		void train(SupervisedDataSet dataset, long contextIdx);*/
		
};
/**************************************************************
*  operator[]
*  using this function will allow you to access the individual
*  contexts. you will also be able to call the context's public
*  functions. It must be passed an integer that is lower than
*  the number of contexts in the burlington. to create new
*  contexts use the newContext() function.
***************************************************************/
template <class T> 
Context<T> & Burlington<T>::operator[](const int contextNumber)
{
    return BurConList[contextNumber];
}


/**************************************************************
*  newContext( norm , dimension )
*  use this to create a new context in the burlington. note
*  that the burlington is empty when it is declared. pass this
*  function the norm you would like to use. pass 1 for L1 and 3 for
*  Lsup. also pass the dimension or number of inputs for the
*  context i.e. iris = 4, sin = 256
***************************************************************/
template <class T>
void Burlington<T>::newContext(char norm, long dimension)
{
	Context<T> temp;
	temp.set_norm(norm);
	temp.set_dimension(dimension);
	BurConList.push_back(temp);
	cout << " setting newContext " << (int) norm << " dim " << dimension << endl;
}


//////////////////////////////////////////////////////////

#define ITYPE 1
#define FTYPE 2
#define DTYPE 3



long getMaxAif(void);
int  getMinAif(void);
void setAifs( int minaif, long maxaif);

#endif
