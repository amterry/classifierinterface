// **************************************************************************
// BarbeauClassifier - Output Interface for the Barbeau Classifier
// 
// Creation date:	January 31, 2014
// Written by:		Chris Crase
// File:		$BarbeauClassifierOutput.h
// Current version:	$Revision: 0.0 $
// Last Modified on:	$Date: 2014/01/31 $
// 
// Copyright (C) 2014, Intel Corp.
// 
// **************************************************************************
// DESCRIPTION:
// Output Interface with the Barbeau Classifier described in the Barbeau ISD 
//
// Usage: N/A
//
// NOTE: N/A
// 
// **************************************************************************

#ifndef BARBEAU_OUTPUT_H
#define BARBEAU_OUTPUT_H

#include <pthread.h>

typedef struct {

    int state;
    int channel;
    pthread_mutex_t lockArray;

} barbeauOutput;

#endif // BARBEAU_OUTPUT_H

