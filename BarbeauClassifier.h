// **************************************************************************
// BarbeauClassifier - Barbeau Classifier
// 
// Creation date:	January 31, 2014
// Written by:		Chris Crase
// File:		$BarbeauClassifier.h
// Current version:	$Revision: 0.0 $
// Last Modified on:	$Date: 2014/01/31 $
// 
// Copyright (C) 2014, Intel Corp.
// 
// **************************************************************************
// DESCRIPTION:
// Barbeau Classifier 
//
// Usage: BarbeauClassifier<barbeauInput> <barbeauOutput> 
//
// NOTE: Return Error Codes  0 - NORMAL, -1....-15 - Errors
// 
// **************************************************************************

#ifndef BARBEAU_CLASSIFIER_H
#define BARBEAU_CLASSIFIER_H

#include "BarbeauClassifierInput.h"
#include "BarbeauClassifierOutput.h"

void BarbeauClassifier(const barbeauInput *in, barbeauOutput *out);

#endif // BARBEAU_CLASSIFIER_H

