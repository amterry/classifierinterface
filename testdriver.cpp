///  
///  testdriver.cpp
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <cstdlib>

#include "BarbeauClassifier.h"
#include "BarbeauClassifierInput.h"
#include "BarbeauClassifierOutput.h"

#define N_SVALS  300000

#define TRAIN_IT 1
#define CLASSIFY_IT 2

using namespace std;

short Svec[N_SVALS];

int main()
{

  barbeauInput testInput;
  barbeauOutput testOutput;

  int i;


  cout << "In Main driver " << endl;

  // init vector

  for (i = 0; i < N_SVALS; i++)
    Svec[i] = 1;


  cout << "In driver " << endl;


  testInput.rawDataSize = 250000; // about one sec's worth of data

  testInput.rawData = (short *) Svec;

  testInput.channel = 1;

///    get a list of data files

///    for all files -- channels -- do the classification

////   read in some vibration data ----  short type
////


///  call the classifier  for Barbeau Analytics

     testInput.mode = TRAIN_IT;
     testInput.tcat = 1;

     BarbeauClassifier(&testInput, &testOutput);

///  check the output classification
     cout << "\n\n The input was trained as " << testInput.tcat << endl;


     testInput.mode = CLASSIFY_IT;
     BarbeauClassifier(&testInput, &testOutput);

     cout << "\n\n The input was classified as " << testOutput.state << endl;

}

