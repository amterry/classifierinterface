// **************************************************************************
// BarbeauClassifier - Input Interface for the Barbeau Classifier
// 
// Creation date:	January 31, 2014
// Written by:		Chris Crase
// File:		$BarbeauClassifierInput.h
// Current version:	$Revision: 0.0 $
// Last Modified on:	$Date: 2014/01/31 $
// 
// Copyright (C) 2014, Intel Corp.
// 
// **************************************************************************
// DESCRIPTION:
// Input to Interface with Barbeau Classifier as decribed in the BarbeauISD 
//
// Usage: N/A
//
// NOTE: N/A
// 
// **************************************************************************

#ifndef BARBEAU_INPUT_H
#define BARBEAU_INPUT_H

#include <pthread.h>

typedef struct {

    int rawDataSize;
    int channel;
    short *rawData;
    int mode;    
    int tcat;
    pthread_mutex_t lockArray;

} barbeauInput;

#endif // BARBEAU_INPUT_H

